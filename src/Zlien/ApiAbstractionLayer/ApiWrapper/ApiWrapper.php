<?php
/**
 * Created by PhpStorm.
 * User: amir
 * Date: 2/21/18
 * Time: 5:26 PM
 */

namespace Zlien\ApiAbstractionLayer\ApiWrapper;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Psr7\Response;
use Psr\Log\LoggerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Zlien\ApiAbstractionLayer\ApiWrapper\Object\ApiRequest;
use Zlien\ApiAbstractionLayer\ApiWrapper\Object\ApiResponse;

/**
 * Class ApiWrapper
 *
 * @package Zlien\ApiAbstractionLayer\ApiWrapper
 */
abstract class ApiWrapper
{
    /**
     * @var HttpClient
     */
    protected $httpClient;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var array
     */
    protected $defaultHeaders;

    /**
     * @var int
     */
    protected $timeout;

    /**
     * ApiWrapper constructor.
     *
     * @param string          $baseUrl
     * @param LoggerInterface $logger
     * @param                 $defaultHeaders
     */
    public function __construct(
        string $baseUrl,
        LoggerInterface $logger,
        $defaultHeaders = [],
        $apiConnectionTimeout = 0
    ) {
        $this->httpClient     = new HttpClient();
        $this->logger         = $logger;
        $this->baseUrl        = $baseUrl;
        $this->defaultHeaders = $defaultHeaders;
        $this->timeout        = $apiConnectionTimeout;
    }

    /**
     * @param ApiRequest $apiRequest
     *
     * @return ApiResponse
     */
    public function connect(ApiRequest $apiRequest): ApiResponse
    {
        try {

            // Check request method
            $method = strtoupper($apiRequest->getMethod());
            if (!in_array($method, ['GET', 'POST', 'DELETE', 'HEAD', 'OPTIONS', 'PATCH', 'PUT'])) {
                throw new Exception('Request method [' . $method . '] not allowed');
            }

            // Make api request and get response
            $response = $this->httpClient->{$method}(
                $this->baseUrl . $apiRequest->getEndpoint(),
                $this->buildOptions(
                    $apiRequest->getQuery(),
                    $apiRequest->getBody(),
                    $apiRequest->getFiles(),
                    $apiRequest->getExtraHeaders(),
                    $this->timeout
                )
            );

            return new ApiResponse($response);
        } catch (RequestException $exception) {

            // Route any api exceptions to the concrete classes error handler
            return $this->handleApiErrors($exception);
        }
    }

    /**
     * @param RequestException $exception
     *
     * @return ApiResponse
     */
    protected function handleApiErrors(RequestException $exception): ApiResponse
    {
        // Log api error encountered
        $exceptionMessage =
            '[' . __CLASS__ . '][' . __FUNCTION__ . '] Request exception with message: ' . $exception->getMessage();
        $responseError    = $exception->getResponse();
        if (!empty($responseError)) {
            $exceptionMessage .= ', and body content: ' . $responseError->getBody()->getContents();
        }
        $this->logger->error($exceptionMessage, $exception->getTrace());

        // Create new api response object from guzzle response and return it
        if ($responseError instanceof Response) {

            return new ApiResponse($responseError);
        } else {

            return new ApiResponse(new Response(500));
        }
    }

    /**
     * Appends the params to the url query
     *
     * @param $url
     * @param $params
     *
     * @return string
     */
    protected function buildUrlWithParams($url, $params)
    {
        $query = http_build_query($params, null, '&', \PHP_QUERY_RFC3986);
        $query = trim($query, '?&');
        if ($query) {
            $glue = strstr($url, '?') === false ? '?' : '&';

            return $url . $glue . $query;
        }

        return $url;
    }

    /**
     * Build options for the guzzle http client.
     *
     * @param null  $query
     * @param null  $body
     * @param null  $files
     * @param array $extraHeaders
     * @param int   $timeout
     *
     * @return array
     */
    private function buildOptions($query = null, $body = null, $files = null, $extraHeaders = [], $timeout = 0)
    {
        $options            = [];
        $options['headers'] = array_merge(
            $this->defaultHeaders,
            $extraHeaders
        );

        // Remove headers that passed the value null to be able to unset one of the headers already set before
        // including the default headers
        foreach ($options['headers'] as $headerName => $headerValue) {
            if (is_null($headerValue)) {
                unset($options['headers'][$headerName]);
            }
        }
        if ($files) {
            $options['multipart'] = [];
            foreach ($files as $file) {
                $options['multipart'][] = [
                    'name'     => $file['file_key'],
                    'contents' => fopen($file['file_content'], 'r'),
                    'filename' => $file['file_name'],
                    'headers'  => [
                        'content-type' => 'multipart/form-data',
                    ],
                ];
            }

            // GuzzleHttp receives the body parameters in the same array `multipart` in case the whole request is
            // multipart i.e. has file attachments.
            foreach ($body as $paramName => $paramValue) {
                $options['multipart'][] = [
                    'name'     => $paramName,
                    'contents' => $paramValue,
                ];
            }
        } elseif ($body) {
            if (
                !empty($extraHeaders['Content-Type']) &&
                $extraHeaders['Content-Type'] == 'application/x-www-form-urlencoded'
            ) {
                $options['form_params'] = $body;
            } else {
                $options['body'] = json_encode($body, JSON_PRESERVE_ZERO_FRACTION); // Deprecated in Guzzle 6
            }
            // $options['form_params'] = $body; // Not working - Form Data
            // json will set the request content type to be application/json
        }
        if ($query) {
            $options['query'] = $query;
        }
        if ($timeout) {
            $options['timeout'] = $timeout;
        }

        return $options;
    }

    /**
     * @param string $baseUrl
     */
    protected function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @param array $defaultHeaders
     */
    protected function setDefaultHeaders(array $defaultHeaders)
    {
        $this->defaultHeaders = $defaultHeaders;
    }

    /**
     * @return int
     */
    public function getTimeout(): int
    {
        return $this->timeout;
    }

    /**
     * @param int $timeout
     */
    public function setTimeout(int $timeout)
    {
        $this->timeout = $timeout;
    }
}
