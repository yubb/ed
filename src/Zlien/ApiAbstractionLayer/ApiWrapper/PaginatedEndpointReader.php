<?php
/**
 * Created by PhpStorm.
 * User: aassem
 * Date: 2/28/18
 * Time: 3:50 PM
 */

namespace Zlien\ApiAbstractionLayer\ApiWrapper;

use Zlien\ApiAbstractionLayer\Exception\PaginationException;

/**
 * Class PaginatedEndpointReader
 *
 * @package Zlien\ApiAbstractionLayer\ApiWrapper
 */
class PaginatedEndpointReader
{
    /**
     * This closure receives page number as argument and returns the objects from that page in the api endpoint
     *
     * @var \Closure
     */
    protected $endpointClosure;

    /**
     * @var int
     */
    private $currentPage;

    /**
     * @var bool
     */
    protected $hasMore;

    /**
     * PaginatedApiReader constructor.
     *
     * @param \Closure $endpointClosure
     *
     * @throws \Exception
     */
    public function __construct(\Closure $endpointClosure)
    {
        $reflection = new \ReflectionFunction($endpointClosure);
        $funcParams = $reflection->getParameters();

        // Only set class attributes if closure has one argument of type integer
        if (count($funcParams) === 1 && $funcParams[0]->getType() == 'int') {

            // Set class attributes
            $this->endpointClosure = $endpointClosure;
            $this->currentPage     = 1;
            $this->hasMore         = true;
        } else {

            // Throw exception if closure signature doesn't match
            throw new \Exception('Passed closure must have one parameter of type \'integer\'');
        }
    }

    /**
     * @return array
     */
    public function getNextPage(): array
    {
        $returnedObjects = [];

        // Try getting new objects from the api endpoint
        try {
            if ($this->hasMore) {
                $returnedObjects = ($this->endpointClosure)($this->currentPage);

                // Check if endpoint returned objects
                if (empty($returnedObjects)) {

                    // Set has more to false if returned objects are empty
                    $this->hasMore = false;
                } else {

                    // Increment count if objects are not empty
                    $this->currentPage++;
                }
            }
        } catch (PaginationException $exception) {
            $this->hasMore = false;
        }

        return $returnedObjects;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        $allRecords = [];
        while ($this->hasMore && $currentRecords = $this->getNextPage()) {
            $allRecords = array_merge($allRecords, $currentRecords);
        }

        return $allRecords;
    }
}
