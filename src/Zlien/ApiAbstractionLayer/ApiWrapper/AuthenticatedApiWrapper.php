<?php
/**
 * Created by PhpStorm.
 * User: mostafa
 * Date: 2/26/18
 * Time: 10:41 PM
 */

namespace Zlien\ApiAbstractionLayer\ApiWrapper;

use Psr\Log\LoggerInterface;
use Zlien\ApiAbstractionLayer\Authenticator\AuthenticatorInterface;
use Zlien\ApiAbstractionLayer\ApiWrapper\Object\ApiRequest;
use Zlien\ApiAbstractionLayer\ApiWrapper\Object\ApiResponse;

/**
 * Class AuthenticatedApiWrapper
 *
 * This class acts as a proxy to the ApiWrapper class, it ensures that any Api that requires authentication has its
 * request authenticated before being sent to the Api server
 *
 * @package Zlien\ApiAbstractionLayer\ApiWrapper
 */
abstract class AuthenticatedApiWrapper extends ApiWrapper
{
    /**
     * @var AuthenticatorInterface
     */
    protected $authenticator;

    /**
     * AuthenticatedApiWrapper constructor.
     *
     * @param string                 $baseUrl
     * @param LoggerInterface        $logger
     * @param AuthenticatorInterface $authenticator
     * @param array                  $defaultHeaders
     * @param int                    $apiConnectionTimeout
     */
    public function __construct(
        $baseUrl,
        LoggerInterface $logger,
        AuthenticatorInterface $authenticator,
        $defaultHeaders = [],
        $apiConnectionTimeout = 0
    ) {
        parent::__construct($baseUrl, $logger, $defaultHeaders, $apiConnectionTimeout);

        // Set class dependencies
        $this->authenticator = $authenticator;
    }

    /**
     * This method
     *
     * @param ApiRequest $apiRequest
     * @param array      $connectionParams
     * @param            $userId
     *
     * @return ApiResponse
     */
    public function authenticatedConnect(
        ApiRequest $apiRequest,
        array $connectionParams,
        $userId
    ) {
        // @TODO wrap in try catch to catch any authentication exception
        $authenticationParams = $this->authenticator->getAuthenticationParams($connectionParams);
        $freshAuthParams      = $this->authenticator->getUpdatedConnectionParameters($authenticationParams);
        if (empty($freshAuthParams)) {
            $freshAuthParams = $authenticationParams;
        }
        $apiRequest = $this->authenticator->prepareAuthenticatedRequest($apiRequest, $freshAuthParams);

        // Update parameters in the database if they have been refreshed
        if ($freshAuthParams !== $authenticationParams) {
            $this->updateConnectionParams($freshAuthParams, $userId);
        }

        return $this->connect($apiRequest);
    }

    /**
     * This function is responsible for updating connection params in database.
     *
     * @param array      $authenticationParams
     * @param            $userId
     */
    abstract public function updateConnectionParams(array $authenticationParams, $userId);
}
