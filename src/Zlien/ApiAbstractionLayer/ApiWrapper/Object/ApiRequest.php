<?php
/**
 * Created by PhpStorm.
 * User: mostafa
 * Date: 2/27/18
 * Time: 12:53 PM
 */

namespace Zlien\ApiAbstractionLayer\ApiWrapper\Object;

/**
 * Class ApiRequest
 *
 * @package Zlien\ApiAbstractionLayer\ApiWrapper\Object
 */
class ApiRequest
{
    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $endpoint;

    /**
     * @var array
     */
    protected $query;

    /**
     * @var array
     */
    protected $body;

    /**
     * @var array
     */
    protected $files;

    /**
     * @var array
     */
    protected $extraHeaders;

    /**
     * ApiRequest constructor.
     *
     * @param string $method
     * @param string $endpoint
     * @param array  $query
     * @param array  $body
     * @param array  $files
     * @param array  $extraHeaders
     * @param int    $timeout
     */
    public function __construct(
        string $method,
        string $endpoint,
        array $query = [],
        array $body = [],
        array $files = [],
        array $extraHeaders = []
    ) {
        $this->method       = $method;
        $this->endpoint     = $endpoint;
        $this->query        = $query;
        $this->body         = $body;
        $this->files        = $files;
        $this->extraHeaders = $extraHeaders;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod(string $method)
    {
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function getEndpoint(): string
    {
        return $this->endpoint;
    }

    /**
     * @param string $endpoint
     */
    public function setEndpoint(string $endpoint)
    {
        $this->endpoint = $endpoint;
    }

    /**
     * @return array
     */
    public function getQuery(): array
    {
        return $this->query;
    }

    /**
     * @param array $query
     */
    public function setQuery(array $query)
    {
        $this->query = $query;
    }

    /**
     * @return array
     */
    public function getBody(): array
    {
        return $this->body;
    }

    /**
     * @param array $body
     */
    public function setBody(array $body)
    {
        $this->body = $body;
    }

    /**
     * @return array
     */
    public function getFiles(): array
    {
        return $this->files;
    }

    /**
     * @param array $files
     */
    public function setFiles(array $files)
    {
        $this->files = $files;
    }

    /**
     * @return array
     */
    public function getExtraHeaders(): array
    {
        return $this->extraHeaders;
    }

    /**
     * @param array $extraHeaders
     */
    public function setExtraHeaders(array $extraHeaders)
    {
        $this->extraHeaders = $extraHeaders;
    }
}
