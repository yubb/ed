<?php
/**
 * Created by PhpStorm.
 * User: mostafa
 * Date: 2/26/18
 * Time: 11:31 PM
 */

namespace Zlien\ApiAbstractionLayer\ApiWrapper\Object;

use GuzzleHttp\Psr7\Response;

/**
 * Class ApiResponse
 *
 * This class acts as a wrapper for the Guzzle response, it allows us to apply multiple adaptations to its contents
 *
 * @package Zlien\ApiAbstractionLayer\ApiWrapper\Object
 */
class ApiResponse
{
    /**
     * @var Response
     */
    public $response;

    /**
     * ApiResponse constructor.
     *
     * @param Response $response
     */
    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * @return bool
     */
    public function isSuccessful()
    {
        $statusCode = $this->response->getStatusCode();

        return ($statusCode >= 200 && $statusCode < 300);
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function getResponseHeader(string $key)
    {
        return $this->response->getHeader($key);
    }

    /**
     * @return mixed
     */
    public function getResponseHeaders()
    {
        return $this->response->getHeaders();
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return json_decode($this->response->getBody()->getContents(), true);
    }

    /**
     * @return array
     */
    public function getResultArray()
    {
        return (array)json_decode($this->response->getBody()->getContents(), true);
    }

    /**
     * @return string
     */
    public function encodeResult()
    {
        return json_encode($this->response->getBody()->getContents());
    }

    /**
     * @return bool|string
     */
    public function getRawResponseContents()
    {
        return $this->response->getBody()->getContents();
    }

    /**
     * @param ApiResponse $response
     *
     * @return bool
     */
    public function isResponseXml()
    {
        // Fetch the Content Type from the header params
        $responseContentType = $this->getResponseHeader('Content-Type');

        // Validate response code is 200 and Content-Type is text/xml
        if (count($responseContentType) > 0 && $responseContentType[0] === 'text/xml; charset=UTF-8') {
            return true;
        }

        return false;
    }
}
