<?php
/**
 * Created by PhpStorm.
 * User: mostafa
 * Date: 2/21/18
 * Time: 2:02 PM
 */

namespace Zlien\ApiAbstractionLayer\Authenticator;

use Psr\Log\LoggerInterface;
use Zlien\ApiAbstractionLayer\ApiWrapper\ApiWrapper;
use Zlien\ApiAbstractionLayer\ApiWrapper\Object\ApiRequest;

/**
 * Class OauthAuthenticator
 *
 * @package Zlien\ApiAbstractionLayer\Authenticator
 */
abstract class OauthAuthenticator extends ApiWrapper implements AuthenticatorInterface
{
    /**
     * Client ID for the provider service
     *
     * @var
     */
    protected $clientId;

    /**
     * Client secret for the provider service
     *
     * @var
     */
    protected $clientSecret;

    /**
     * Redirect Uri for the provider service
     *
     * @var
     */
    protected $redirectUri;

    /**
     * OauthAuthenticator constructor.
     *
     * @param string          $baseUrl
     * @param LoggerInterface $logger
     * @param string          $clientId
     * @param string          $clientSecret
     * @param string          $redirectUri
     */
    public function __construct(
        string $baseUrl,
        LoggerInterface $logger,
        string $clientId,
        string $clientSecret,
        string $redirectUri
    ) {
        parent::__construct($baseUrl, $logger);

        $this->clientId     = $clientId;
        $this->clientSecret = $clientSecret;
        $this->redirectUri  = $redirectUri;
    }

    /**
     * This function carries out the first time authentication with the external API
     *
     * @param       $externalUserId
     * @param array $params
     *
     * @return mixed
     */
    public function authenticate($externalUserId, $params = [])
    {
        $responseData = false;

        if (!empty($params['code'])) {

            // get access token using code
            $responseData = $this->requestAccessToken($params['code']);
        }

        return $responseData;
    }

    /**
     * @param ApiRequest $apiRequest
     * @param array      $authenticationParams
     *
     * @return ApiRequest
     */
    public function prepareAuthenticatedRequest(ApiRequest $apiRequest, array $authenticationParams): ApiRequest
    {
        // Add oauth authorization header to extra headers
        $authorizationHeader = ['authorization' => 'Bearer ' . $authenticationParams['access_token']];
        $apiRequest->setExtraHeaders(array_merge($apiRequest->getExtraHeaders(), $authorizationHeader));

        return $apiRequest;
    }

    /**
     * Generate random state value
     *
     * @param int $length
     *
     * @return string
     */
    protected function getRandomState($length = 32)
    {
        return bin2hex(random_bytes($length / 2));
    }

    /**
     * Provides the authorization url for 3-legged oauth2 connection
     *
     * @return string
     */
    public function getAuthorizationUrl()
    {
        $url    = $this->baseUrl . $this->getAuthorizationEndpoint();
        $params = [
            'response_type' => 'code',
            'redirect_uri'  => $this->redirectUri,
            'state'         => $this->getRandomState(),
            'client_id'     => $this->clientId,
        ];
        $url    = $this->buildUrlWithParams($url, $params);

        return $url;
    }

    /**
     * Function that requests access token using authoriation code
     *
     * @param       $authorizationCode
     * @param array $params
     *
     * @return mixed
     */
    abstract protected function requestAccessToken($authorizationCode = null, $params = []);

    /**
     * Function that refreshes access token using existing tokens
     *
     * @param $accessToken
     * @param $refreshToken
     *
     * @return mixed
     */
    abstract protected function refreshAccessToken($accessToken, $refreshToken);

    /**
     * Function that retreives the authorization endpoint relative to the provider base url
     *
     * @return mixed
     */
    abstract protected function getAuthorizationEndpoint();

    /**
     * Function that retreives the access token endpoint relative to the provider base url
     *
     * @return mixed
     */
    abstract protected function getAccessTokenEndpoint();
}
