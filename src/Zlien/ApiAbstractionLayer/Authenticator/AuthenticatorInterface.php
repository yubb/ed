<?php
/**
 * Created by PhpStorm.
 * User: mostafa
 * Date: 2/21/18
 * Time: 12:52 PM
 */

namespace Zlien\ApiAbstractionLayer\Authenticator;

use Zlien\ApiAbstractionLayer\ApiWrapper\Object\ApiRequest;

/**
 * Interface AuthenticatorInterface
 *
 * @package Zlien\IntegrationBundle\Authenticator
 */
interface AuthenticatorInterface
{
    /**
     * This function carries out the first time authentication with the external API
     *
     * @param       $externalUserId
     * @param array $params
     *
     * @return mixed
     */
    public function authenticate($externalUserId, $params = []);

    /**
     * This function extracts and returns authentication params from the connection params
     *
     * @param array $connectionParams
     *
     * @return array
     */
    public function getAuthenticationParams(array $connectionParams): array;

    /**
     *
     * This function checks for the expiry of the existing tokens and refresh them if expired.
     *
     * @param array $params
     *
     * @param bool  $forceUpdate
     *
     * @return array
     */
    public function getUpdatedConnectionParameters(array $params, bool $forceUpdate = false): array;

    /**
     * The responsibility of this function is create an authenticated request to be sent to the external service
     * using the authentication params
     *
     * @param ApiRequest $apiRequest
     * @param array      $authenticationParams The authentication parameters used by the authentication type
     *
     * @return ApiRequest
     */
    public function prepareAuthenticatedRequest(ApiRequest $apiRequest, array $authenticationParams): ApiRequest;
}
