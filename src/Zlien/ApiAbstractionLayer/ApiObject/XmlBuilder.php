<?php
/**
 * Created by PhpStorm.
 * User: mostafa
 * Date: 4/23/18
 * Time: 3:48 PM
 */

namespace Zlien\ApiAbstractionLayer\ApiObject;

/**
 * Interface XmlBuilder
 *
 * @package Zlien\ApiAbstractionLayer\ApiObject
 */
interface XmlBuilder
{
    /**
     * @param \SimpleXMLElement $element
     *
     * @return $this
     */
    public static function buildFromXml(\SimpleXMLElement $element);
}
