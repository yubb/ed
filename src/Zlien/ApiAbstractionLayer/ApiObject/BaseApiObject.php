<?php
/**
 * Created by PhpStorm.
 * User: mostafa
 * Date: 2/27/18
 * Time: 8:52 PM
 */

namespace Zlien\ApiAbstractionLayer\ApiObject;

use Zlien\ApiAbstractionLayer\Converter\ConventionConverterTrait;

/**
 * Class BaseApiObject
 *
 * @package Zlien\ApiAbstractionLayer\ApiObject
 */
abstract class BaseApiObject
{
    use ConventionConverterTrait;

    /**
     * @return string
     */
    public function __toString()
    {
        $result = '[' . get_class($this) . '] ' . ' {';
        foreach ($this as $attribute => $value) {
            if (is_array($value)) {
                $value = json_encode($value);
            } else {
                $value = '"' . $value . '"';
            }
            $result .= '"' . $attribute . '" => ' . $value;
            if ($value !== end($this)) {
                $result .= ', ';
            }
        }
        $result .= '}';

        return $result;
    }

    /**
     * @param $an_array
     */
    public static function __set_state($an_array)
    {
        // TODO: Implement __set_state() method.
    }
}
