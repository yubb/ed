<?php

/**
 * Created by PhpStorm.
 * User: mostafa
 * Date: 6/6/18
 * Time: 12:23 AM
 */

namespace Zlien\ApiAbstractionLayer\Exception;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class PaginationException
 *
 * @package Zlien\ApiAbstractionLayer\Exception
 */
class PaginationException extends \Exception
{
    /**
     * PaginationException constructor.
     */
    public function __construct()
    {
        parent::__construct('Requested invalid page', Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
