<?php
/**
 * Created by PhpStorm.
 * User: mostafa
 * Date: 2/22/18
 * Time: 10:37 PM
 */

namespace Zlien\ApiAbstractionLayer\Converter;

/**
 * Trait ConventionConverterTrait
 *
 * @package Zlien\ApiAbstractionLayer\Converter
 */
trait ConventionConverterTrait
{
    /**
     * @param array $dataArray
     */
    public function initWithArray(array $dataArray)
    {
        foreach ($dataArray as $propertyName => $propertyValue) {

            // Convert snake case to lower camel case names
            $camelCaseName = lcfirst(str_replace('_', '', ucwords($propertyName, '_')));

            // Set property if exists in object
            if (property_exists($this, $camelCaseName) && !is_null($propertyValue)) {
                $this->$camelCaseName = $propertyValue;
            }
        }
    }

    /**
     * @param array $specialMapping
     *
     * @return array
     */
    public function getSnakeCaseArray(array $specialMapping = []): array
    {
        $result = array();

        // Convert the class properties to an array format key => value
        foreach ($this as $key => $value) {
            $key = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $key));

            // Check if special mapping exists for this array key, and use if there is
            if (array_key_exists($key, $specialMapping)) {
                $key = $specialMapping[$key];
            }
            $result[$key] = $value;
        }

        return $result;
    }
}
