<?php
/**
 * Created by PhpStorm.
 * User: amir
 * Date: 11/24/18
 * Time: 5:22 PM
 */

namespace Zlien\ExternalDataProviderLayer\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Zlien\ApiAbstractionLayer\ApiWrapper\ApiWrapper;
use Zlien\ExternalDataProviderLayer\Adapter\RequestAdapter;
use Zlien\ExternalDataProviderLayer\Adapter\ResponseAdapter;
use Zlien\ExternalDataProviderLayer\ApiWrapper\ProviderApiWrapper;

/**
 * Class ProxyRequestHandlerService
 *
 * @package Zlien\ExternalDataProviderLayer\Service
 */
class ProxyRequestHandlerService implements RequestHandlerInterface
{
    /**
     * @var RequestAdapter
     */
    protected $requestAdapter;

    /**
     * @var ProviderApiWrapper
     */
    protected $providerApiWrapper;

    /**
     * @var ResponseAdapter
     */
    protected $responseAdapter;

    /**
     * ProxyRequestHandlerService constructor.
     *
     * @param RequestAdapter  $requestAdapter
     * @param ApiWrapper      $apiWrapper
     * @param ResponseAdapter $responseAdapter
     */
    public function __construct(
        RequestAdapter $requestAdapter,
        ProviderApiWrapper $providerApiWrapper,
        ResponseAdapter $responseAdapter
    ) {
        $this->requestAdapter     = $requestAdapter;
        $this->providerApiWrapper = $providerApiWrapper;
        $this->responseAdapter    = $responseAdapter;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function handleRequest(Request $request): Response
    {
        // @TODO handle failure
        $apiRequest  = $this->requestAdapter->httpRequestToApiRequest($request);
        $apiResponse = $this->providerApiWrapper->connect($apiRequest);

        return $this->responseAdapter->apiResponseToHttpResponse($apiResponse);
    }
}
