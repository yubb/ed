<?php
/**
 * Created by PhpStorm.
 * User: amir
 * Date: 11/27/18
 * Time: 11:46 AM
 */

namespace Zlien\ExternalDataProviderLayer\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Interface RequestHandlerInterface
 *
 * @package Zlien\ExternalDataProviderLayer\Service
 */
interface RequestHandlerInterface
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function handleRequest(Request $request): Response;
}
