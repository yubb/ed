<?php
/**
 * Created by PhpStorm.
 * User: amir
 * Date: 11/22/18
 * Time: 4:43 PM
 */

namespace Zlien\ExternalDataProviderLayer\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Zlien\ExternalDataCacheBundle\Service\RequestCachingService;
use Zlien\ExternalDataProviderLayer\Service\ProxyRequestHandlerService;

class ExternalDataProviderController extends Controller
{
    /**
     * @var RequestCachingService
     */
    protected $requestCachingService;

    /**
     * ExternalDataProviderController constructor.
     *
     * @param ProxyRequestHandlerService $proxyRequestHandlerService
     */
    public function __construct(RequestCachingService $requestCachingService)
    {
        $this->requestCachingService      = $requestCachingService;
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function endpointAction(Request $request)
    {
        return $this->requestCachingService->cacheResponse($request);
    }
}
