<?php
/**
 * Created by PhpStorm.
 * User: amir
 * Date: 11/25/18
 * Time: 7:22 AM
 */

namespace Zlien\ExternalDataProviderLayer\ApiWrapper;

use Psr\Log\LoggerInterface;
use Zlien\ApiAbstractionLayer\ApiWrapper\ApiWrapper;

/**
 * Class ProviderApiWrapper
 *
 * @package Zlien\ExternalDataProviderLayer\ApiWrapper
 */
class ProviderApiWrapper extends ApiWrapper
{
    /**
     * ProviderApiWrapper constructor.
     *
     * @param string          $baseUrl
     * @param LoggerInterface $logger
     * @param array           $defaultHeaders
     * @param int             $apiConnectionTimeout
     */
    public function __construct(
        string $baseUrl,
        LoggerInterface $logger,
        array $defaultHeaders = [],
        int $apiConnectionTimeout = 0
    ) {
        parent::__construct($baseUrl, $logger, $defaultHeaders, $apiConnectionTimeout);
    }
}
