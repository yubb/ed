<?php
/**
 * Created by PhpStorm.
 * User: amir
 * Date: 11/22/18
 * Time: 5:43 PM
 */

namespace Zlien\ExternalDataProviderLayer\Adapter;

use Symfony\Component\HttpFoundation\Request;
use Zlien\ApiAbstractionLayer\ApiWrapper\Object\ApiRequest;

/**
 * Class RequestAdapter
 *
 * @package Zlien\ExternalDataProviderLayer\Adapter
 */
class RequestAdapter
{
    /**
     * Adapt from HttpFoundation Request object to ApiAbstractionLayer Request
     *
     * @param Request $request
     *
     * @return ApiRequest
     */
    public function httpRequestToApiRequest(Request $request)
    {
        $httpMethod = $request->getRealMethod();
        parse_str(urldecode($request->getQueryString()), $queryString);
        $headers = $this->filterRequestHeaders($request->headers->all());
        $body    = (array) json_decode($request->getContent(), true);
        if (!empty($queryString) && array_key_exists('endpoint', $queryString)) {
            $endpoint = $queryString['endpoint'];
            unset($queryString['endpoint']);
        } else {
            // @TODO handle no endpoint
        }

        return new ApiRequest($httpMethod, $endpoint, $queryString, $body, [], $headers);
    }

    /**
     * Remove unneeded headers from the headers array
     *
     * @param array $headers
     *
     * @return array
     */
    protected function filterRequestHeaders(array $headers)
    {
        // header names to be removed in lowercase
        $headersToRemove = ['host'];
        foreach ($headers as $name => $value) {
            if (in_array(strtolower($name), $headersToRemove)) {
                unset($headers[$name]);
            }
        }

        return $headers;
    }
}
