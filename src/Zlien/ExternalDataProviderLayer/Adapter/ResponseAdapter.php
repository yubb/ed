<?php
/**
 * Created by PhpStorm.
 * User: amir
 * Date: 11/24/18
 * Time: 5:30 PM
 */

namespace Zlien\ExternalDataProviderLayer\Adapter;

use Symfony\Component\HttpFoundation\Response;
use Zlien\ApiAbstractionLayer\ApiWrapper\Object\ApiResponse;

/**
 * Class ResponseAdapter
 *
 * @package Zlien\ExternalDataProviderLayer\Adapter
 */
class ResponseAdapter
{
    /**
     * Adapt from ApiAbstractionLayer Response to HttpFoundation Response object
     *
     * @param ApiResponse $apiResponse
     *
     * @return Response
     */
    public function apiResponseToHttpResponse(ApiResponse $apiResponse): Response
    {
        // @TODO check why adding response headers make the response fail
        $statusCode      = $apiResponse->response->getStatusCode();
        // $responseHeaders = $this->filterResponseHeaders($apiResponse->getResponseHeaders());
        $responseBody    = $apiResponse->getRawResponseContents();

        return new Response($responseBody, $statusCode);
    }

    /**
     * @param array $headers
     *
     * @return array
     */
    protected function filterResponseHeaders(array $headers)
    {
        // header names to be removed in lowercase
        $headersToRemove = ['set-cookie', 'date', 'expect-ct'];
        foreach ($headers as $name => $value) {
            if (in_array(strtolower($name), $headersToRemove)) {
                unset($headers[$name]);
            }
        }

        return $headers;
    }
}
