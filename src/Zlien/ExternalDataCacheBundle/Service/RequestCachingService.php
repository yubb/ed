<?php
/**
 * Created by PhpStorm.
 * User: amir
 * Date: 11/25/18
 * Time: 4:08 PM
 */

namespace Zlien\ExternalDataCacheBundle\Service;

use Symfony\Component\HttpFoundation\Request;
use Zlien\ExternalDataCacheBundle\Entity\Response;
use Zlien\ExternalDataCacheBundle\Repository\ResponseRepository;
use Zlien\ExternalDataProviderLayer\Service\RequestHandlerInterface;

/**
 * Class RequestCachingService
 *
 * @package Zlien\ExternalDataCacheBundle\Service
 */
class RequestCachingService
{
    /**
     * @var ResponseRepository
     */
    protected $responseRepository;

    /**
     * @var RequestHandlerInterface
     */
    protected $requestHandler;

    /**
     * RequestCachingService constructor.
     *
     * @param ResponseRepository $responseRepository
     */
    public function __construct(ResponseRepository $responseRepository, RequestHandlerInterface $requestHandler)
    {
        $this->responseRepository = $responseRepository;
        $this->requestHandler     = $requestHandler;
    }

    /**
     * @param Request $request
     *
     * @return object|null
     */
    public function getCachedResponse(Request $request)
    {
        // @TODO handle expired responses by checking lastUpdated value against value from configuration
        $hash = $this->hashRequest($request);

        return $this->responseRepository->findOneBy(['hash' => $hash]);
    }

    /**
     * @param Request $request
     * @param         $responseBody
     *
     * @return Response
     * @throws \Exception
     */
    public function cacheResponse(Request $request)
    {
        $response = $this->getCachedResponse($request);
        if (empty($response)) {
            $httpResponse = $this->requestHandler->handleRequest($request);

            // @TODO handle if the response was unsuccessful
            $response = new Response();
            $response->setResponse(serialize($httpResponse));
            $response->setLastUpdated(new \DateTime());
            $response->setHash($this->hashRequest($request));
            $this->responseRepository->insertUpdate($response);
        } else {
            $httpResponse = unserialize($response->getResponse());
        }

        return $httpResponse;
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    private function hashRequest(Request $request)
    {
        // @TODO verify if MD5 is the best hashing algorithm for our use case
        // @TODO update hash field length
        // MD5 can handle input up to 2^64 length
        return md5(serialize($request));
    }
}
